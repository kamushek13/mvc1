package domain

import (
	"gitlab.com/kamushek13/mvc1/utils"
	"net/http"
)

var users = map[int64]*User{
	13: {
		13,
		"Assyltas",
		"Armanova",
		"armanova13a@gmail.com",
	},
	3: {
		3,
		"Kamushek",
		"Frank",
		"rmna13a@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "User not found",
		Status:  http.StatusNotFound,
		Code:    "not found",
	}

}
