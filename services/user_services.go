package services

import (
	"gitlab.com/kamushek13/mvc1/domain"
	"gitlab.com/kamushek13/mvc1/utils"
)

// user_services
func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
